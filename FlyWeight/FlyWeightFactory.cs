﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WifiCats;

namespace FlyWeight
{
    class FlyWeightFactory
    {
        private Dictionary<KatzenTyp, Katze> katzen = new Dictionary<KatzenTyp, Katze>();
        public Katze GetKatze(KatzenTyp typ)
        {
            if (!katzen.ContainsKey(typ))
                katzen.Add(typ, new Katze(typ, 0, 0));

            return katzen[typ];
        }
    }
}
