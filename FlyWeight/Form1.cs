﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WifiCats;

namespace FlyWeight
{
    public partial class Form1 : Form
    {
        Dictionary<int, KatzenTyp> typen = new Dictionary<int, KatzenTyp>()
        {
            [1] = KatzenTyp.Katze1,
            [2] = KatzenTyp.Katze2,
            [3] = KatzenTyp.Katze3,
            [4] = KatzenTyp.Katze4,
            [5] = KatzenTyp.Katze5
        };
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random r = new Random(DateTime.Now.Millisecond);
            Graphics g = this.CreateGraphics();
            List<Katze> katzen = new List<Katze>();
            int x = 0;
            int y = 30;
            for(int i = 0; i < 1000; i++)
            {
                var typ = typen[r.Next(1, 6)];
                katzen.Add(new Katze(typ, x, y));
                x += 20;
                if (x > this.Width)
                {
                    x = 0;
                    y += 20;
                }
            }
            foreach (var k in katzen)
            {
                k.Draw(g);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Random r = new Random(DateTime.Now.Millisecond);
            Graphics g = this.CreateGraphics();

            List<KeyValuePair<KatzenTyp, Point>> katzen = new List<KeyValuePair<KatzenTyp, Point>>();
            int x = 0;
            int y = 30;

            for (int i = 0; i < 1000; i++)
            {
                var typ = typen[r.Next(1, 6)];
                katzen.Add(new KeyValuePair<KatzenTyp, Point>(typ, new Point(x, y)));
                x += 20;
                if (x > this.Width)
                {
                    x = 0;
                    y += 20;
                }
            }

            FlyWeightFactory fc = new FlyWeightFactory();
            foreach(var item in katzen)
            {
                var k = fc.GetKatze(item.Key);
                k.X = item.Value.X;
                k.Y = item.Value.Y;
                k.Draw(g);
            }
            
        }
    }
}
